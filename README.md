# Tux Mod

A Minecraft mod that adds the mascot for the best desktop operating system

[MinecraftForums Page][id/name] 

[id/name]: https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/2911151-tux-mod

# How It Works

Craft a Windows Disk using rose red cacti green dandelion yellow and cyan dyes

![](TuxMod/screenshots/2018-06-29_01.34.50.png)

Use it to summon an angry tux
![](TuxMod/screenshots/2018-06-29_00.20.23.png)
These guys are super tough

Damaging them will cause them to spawn new versions of themselves

Even a diamond sword is too weak to keep up with the respawning
![](TuxMod/screenshots/2018-06-29_00.23.26.png)
The only way to effectively kill them is with a fork bomb

You can craft one with iron and gunpowder

![](TuxMod/screenshots/2018-06-29_01.34.03.png)

They have a chance to drop a Linux Disk
![](TuxMod/screenshots/2018-06-29_00.21.27.png)

These allow you to summon a Tamed Tux

![](TuxMod/screenshots/2018-06-29_00.25.52.png)

These act similarly to wolves

They will follow you around

![](TuxMod/screenshots/2018-06-29_00.26.10.png)

They will also attack anything you attack or anything that attacks you

![](TuxMod/screenshots/2018-06-29_00.27.03.png)

If they happen to die they will drop a new Linux Disk

![](TuxMod/screenshots/2018-06-29_00.28.16.png)
package com.jul2040.tuxmod.items;

import com.jul2040.tuxmod.init.ModItems;
import com.jul2040.tuxmod.util.IHasModel;
import com.jul2040.tuxmod.Main;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ItemBase extends Item implements IHasModel{
	public ItemBase(String name) {
		setUnlocalizedName(name);
		setRegistryName(name);
	}
	@Override
	public void registerModels() {
		Main.proxy.registerItemRenderer(this,0,"inventory");
	}
}
package com.jul2040.tuxmod.items;

import com.jul2040.tuxmod.Entity.EntityTux;
import com.jul2040.tuxmod.Entity.EntityTuxTamed;
import com.jul2040.tuxmod.init.ModItems;

import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class ForkBomb extends ItemBase {

	public ForkBomb(String name) {
		super(name);
		setCreativeTab(CreativeTabs.MISC);
		ModItems.ITEMS.add(this);
		setMaxStackSize(1);
	}
	
	@Override
	public boolean onLeftClickEntity(ItemStack stack, EntityPlayer player, Entity entity) {
		if((entity instanceof EntityTux || entity instanceof EntityTuxTamed) && !player.getEntityWorld().isRemote) {
            Minecraft mc = Minecraft.getMinecraft();
            mc.player.sendMessage(new TextComponentTranslation("$ :(){ :|: & };:"));
			entity.attackEntityFrom(DamageSource.GENERIC, 80);
		}
		return true;
	}
}

package com.jul2040.tuxmod.Entity;

import com.jul2040.tuxmod.util.handlers.LootTableHandler;
import com.jul2040.tuxmod.util.handlers.SoundsHandler;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.ai.EntityAIHurtByTarget;
import net.minecraft.entity.ai.EntityAIMoveThroughVillage;
import net.minecraft.entity.ai.EntityAINearestAttackableTarget;
import net.minecraft.entity.ai.EntityAIWanderAvoidWater;
import net.minecraft.entity.ai.EntityAIWatchClosest;
import net.minecraft.entity.monster.EntityIronGolem;
import net.minecraft.entity.monster.EntityPigZombie;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class EntityTux extends EntityZombie{
	public EntityTux(World worldIn) {
		super(worldIn);
		setSize(0.75F,1.5F);
	}
	@Override
	protected void applyEntityAttributes() {
		super.applyEntityAttributes();
		this.getEntityAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(80.0D);
		this.getEntityAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3D);
		this.getEntityAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(4.0D);
		this.getEntityAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(1000);
	}
	@Override
	public float getEyeHeight() {
		return 1.5F;
	}
	@Override
    protected void applyEntityAI()
    {
        this.targetTasks.addTask(1, new EntityAIHurtByTarget(this, true, new Class[] {EntityPigZombie.class}));
        this.targetTasks.addTask(2, new EntityAINearestAttackableTarget(this, EntityPlayer.class, true));
    }
	@Override
    protected boolean shouldBurnInDay()
    {
        return false;
    }
	@Override
	protected SoundEvent getAmbientSound() {
		return SoundsHandler.ENTITY_TUX_AMBIENT;
	}
	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
		return SoundsHandler.ENTITY_TUX_HURT;
	}
	@Override
	protected SoundEvent getDeathSound() {
		return SoundsHandler.ENTITY_TUX_DEATH;
	}
    Minecraft mc = Minecraft.getMinecraft();
	@Override
	public boolean attackEntityFrom(DamageSource source, float amount) {
		if(rand.nextInt() % 2 == 1 && !this.getEntityWorld().isRemote && source != DamageSource.IN_WALL) {
			double x = source.getDamageLocation().x + rand.nextInt() % 10 - 5;
			double y = source.getDamageLocation().y;
			double z = source.getDamageLocation().z + rand.nextInt() % 10 - 5;
			Block block = this.getEntityWorld().getBlockState(new BlockPos(x, y, z)).getBlock();
			while(this.getEntityWorld().getBlockState(new BlockPos(x, y, z)).getBlock().getIdFromBlock(block) != 0) {
				block = this.getEntityWorld().getBlockState(new BlockPos(x, y, z)).getBlock();
				System.out.println(Integer.toString(this.getEntityWorld().getBlockState(new BlockPos(x, y, z)).getBlock().getIdFromBlock(block)));
				y++;
			}
			EntityTux newTux = new EntityTux(this.getEntityWorld());
			newTux.setLocationAndAngles(x, y, z, 0, 0);
			this.getEntityWorld().spawnEntity(newTux);
		}
		return super.attackEntityFrom(source, amount);
	}
	@Override
	protected ResourceLocation getLootTable() {
		return LootTableHandler.TUX;
	}
}

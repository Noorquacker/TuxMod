package com.jul2040.tuxmod.Entity.Render;

import com.jul2040.tuxmod.Entity.EntityTux;
import com.jul2040.tuxmod.Entity.Model.ModelTux;
import com.jul2040.tuxmod.util.References;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.registry.IRenderFactory;

public class RenderTux extends RenderLiving<EntityTux>{
	public static final ResourceLocation TEXTURES = new ResourceLocation(References.MOD_ID + ":textures/entity/tux.png");
	public RenderTux(RenderManager manager) {
		super(manager, new ModelTux(), .5F);
	}
	@Override
	protected ResourceLocation getEntityTexture(EntityTux entity) {
		// TODO Auto-generated method stub
		return TEXTURES;
	}
    public static class Factory implements IRenderFactory<EntityTux> {

        @Override
        public Render<? super EntityTux> createRenderFor(RenderManager manager) {
            return new RenderTux(manager);
        }

    }
}

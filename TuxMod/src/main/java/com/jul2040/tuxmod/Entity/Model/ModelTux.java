package com.jul2040.tuxmod.Entity.Model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

/**
 * Tux - jul2040
 * Created using Tabula 6.0.0
 */
public class ModelTux extends ModelBase {
    public ModelRenderer head;
    public ModelRenderer body;
    public ModelRenderer bottom_beak;
    public ModelRenderer top_beak;
    public ModelRenderer right_foot;
    public ModelRenderer left_foot;
    public ModelRenderer belly;
    public ModelRenderer right_arm;
    public ModelRenderer left_arm;

    public ModelTux() {
        this.textureWidth = 64;
        this.textureHeight = 32;
        this.left_arm = new ModelRenderer(this, 52, 18);
        this.left_arm.setRotationPoint(-4.8F, 9.1F, 0.0F);
        this.left_arm.addBox(-2.8F, 0.0F, 0.0F, 3, 11, 3, 0.0F);
        this.head = new ModelRenderer(this, 0, 0);
        this.head.setRotationPoint(0.2F, 8.5F, 0.4F);
        this.head.addBox(-3.3F, -6.2F, -3.1F, 7, 7, 7, 0.0F);
        this.right_foot = new ModelRenderer(this, 50, 0);
        this.right_foot.setRotationPoint(-3.9F, 22.2F, -3.0F);
        this.right_foot.addBox(0.0F, 0.0F, 0.0F, 3, 2, 4, 0.0F);
        this.bottom_beak = new ModelRenderer(this, 56, 0);
        this.bottom_beak.setRotationPoint(0.2F, 8.5F, 0.4F);
        this.bottom_beak.addBox(-1.0F, -2.2F, -4.9F, 2, 1, 2, 0.0F);
        this.left_foot = new ModelRenderer(this, 50, 0);
        this.left_foot.setRotationPoint(1.3F, 22.2F, -3.0F);
        this.left_foot.addBox(0.0F, 0.0F, 0.0F, 3, 2, 4, 0.0F);
        this.belly = new ModelRenderer(this, 0, 18);
        this.belly.setRotationPoint(-2.5F, 10.3F, -2.5F);
        this.belly.addBox(0.0F, 0.0F, 0.0F, 6, 10, 4, 0.0F);
        this.body = new ModelRenderer(this, 34, 13);
        this.body.setRotationPoint(-4.7F, 8.5F, -1.3F);
        this.body.addBox(0.0F, 0.0F, 0.0F, 10, 14, 5, 0.0F);
        this.top_beak = new ModelRenderer(this, 50, 0);
        this.top_beak.setRotationPoint(0.2F, 8.5F, 0.4F);
        this.top_beak.addBox(-1.5F, -2.9F, -5.9F, 3, 1, 4, 0.0F);
        this.right_arm = new ModelRenderer(this, 52, 18);
        this.right_arm.setRotationPoint(4.9F, 9.1F, 0.0F);
        this.right_arm.addBox(0.0F, 0.0F, 0.0F, 3, 11, 3, 0.0F);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) { 
        this.left_arm.render(f5);
        this.head.render(f5);
        this.right_foot.render(f5);
        this.bottom_beak.render(f5);
        this.left_foot.render(f5);
        this.belly.render(f5);
        this.body.render(f5);
        this.top_beak.render(f5);
        this.right_arm.render(f5);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }
    @Override
    public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn) 
    {
    	this.right_arm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
    	this.left_arm.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 1.4F * limbSwingAmount;
    	
    	this.head.rotateAngleY = netHeadYaw * 0.017453292F;
    	this.head.rotateAngleX = headPitch * 0.017453292F;
    	this.top_beak.rotateAngleY = netHeadYaw * 0.017453292F;
    	this.top_beak.rotateAngleX = headPitch * 0.017453292F;
    	this.bottom_beak.rotateAngleY = netHeadYaw * 0.017453292F;
    	this.bottom_beak.rotateAngleX = headPitch * 0.017453292F;
}
}

package com.jul2040.tuxmod.init;

import com.jul2040.tuxmod.Entity.EntityTux;
import com.jul2040.tuxmod.Entity.EntityTuxTamed;
import com.jul2040.tuxmod.items.ItemBase;

import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;

public class LinuxDisk extends ItemBase {
	
	public LinuxDisk(String name) {
		super(name);
		setCreativeTab(CreativeTabs.MISC);
		ModItems.ITEMS.add(this);
		setMaxStackSize(1);
	}

	@Override
    public EnumActionResult onItemUse(EntityPlayer player, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ){
		if (!worldIn.isRemote) {
            Minecraft mc = Minecraft.getMinecraft();
            mc.player.sendMessage(new TextComponentTranslation("The power of LINUX is on your side"));
			EntityTuxTamed tux = new EntityTuxTamed(worldIn);
			tux.setOwnerId(player.getPersistentID());
			tux.setLocationAndAngles(pos.getX() + hitX, pos.getY() + hitY, pos.getZ() + hitZ, 0, 0);
			worldIn.spawnEntity(tux);
			if(!player.isCreative()) {
				ItemStack stack = player.getActiveItemStack();
				stack.setCount(0);
				player.setHeldItem(hand, stack);
			}
		}
        return EnumActionResult.PASS;
    }
}

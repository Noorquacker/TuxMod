package com.jul2040.tuxmod.init;

import com.jul2040.tuxmod.Entity.EntityTux;
import com.jul2040.tuxmod.Entity.EntityTuxTamed;
import com.jul2040.tuxmod.util.References;
import com.jul2040.tuxmod.Main;

import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.EntityRegistry;

public class EntityInit {
	public static void registerEntities() {
		registerEntity("Tux", EntityTux.class, References.ENTITY_TUX, 100);
		registerEntity("Tux_Tamed", EntityTuxTamed.class, References.ENTITY_TUX_TAMED, 100);
	}
	private static void registerEntity(String name, Class<? extends Entity> entity, int id, int trackingRange) {
		EntityRegistry.registerModEntity(new ResourceLocation(References.MOD_ID + ':' + name), entity, name, id, Main.Instance, trackingRange, 1, true);
	}
}
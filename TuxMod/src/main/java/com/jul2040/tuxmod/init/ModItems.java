package com.jul2040.tuxmod.init;

import java.util.ArrayList;
import java.util.List;

import com.jul2040.tuxmod.items.ForkBomb;
import com.jul2040.tuxmod.items.ItemBase;
import com.jul2040.tuxmod.items.WindowsDisk;

import net.minecraft.item.Item;

public class ModItems {
	public static final List<Item> ITEMS = new ArrayList<Item>();
	
	public static final Item WindowsDisk = new WindowsDisk("windows_disk");
	public static final Item LinuxDisk = new LinuxDisk("linux_disk");
	public static final Item FORKBOMB = new ForkBomb("fork_bomb");
}

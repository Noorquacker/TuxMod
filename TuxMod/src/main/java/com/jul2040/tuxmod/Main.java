package com.jul2040.tuxmod;
import com.jul2040.tuxmod.init.EntityInit;
import com.jul2040.tuxmod.items.WindowsDisk;
import com.jul2040.tuxmod.proxy.CommonProxy;
import com.jul2040.tuxmod.util.References;
import com.jul2040.tuxmod.util.handlers.RenderHandler;
import com.jul2040.tuxmod.util.handlers.SoundsHandler;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod(modid=References.MOD_ID, name=References.NAME, version=References.VERSION)
public class Main {
	@Instance
	public static Main Instance;
	
	@SidedProxy(clientSide=References.CLIENT_PROXY, serverSide=References.COMMON_PROXY)
	public static CommonProxy proxy;
	
	@EventHandler
	public static void PreInit(FMLPreInitializationEvent event) {
		EntityInit.registerEntities();
		RenderHandler.registerEntityRenders();
	}
	@EventHandler
	public static void Init(FMLInitializationEvent event) {
		SoundsHandler.registerSounds();
	}
	@EventHandler
	public static void PostInit(FMLPostInitializationEvent event) {
		
	}
	@EventHandler
	public static void ItemTossEvent(FMLEvent event) {
		
	}
}
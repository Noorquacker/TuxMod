package com.jul2040.tuxmod.util.handlers;

import com.jul2040.tuxmod.util.References;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class SoundsHandler {
	public static SoundEvent ENTITY_TUX_AMBIENT, ENTITY_TUX_HURT, ENTITY_TUX_DEATH;
	
	public static void registerSounds() {
		ENTITY_TUX_AMBIENT = registerSound("entity.tux.ambient");
		ENTITY_TUX_HURT = registerSound("entity.tux.hurt");
		ENTITY_TUX_DEATH = registerSound("entity.tux.death");
	}
	private static SoundEvent registerSound(String name) {
		ResourceLocation location = new ResourceLocation(References.MOD_ID, name);
		SoundEvent event = new SoundEvent(location);
		event.setRegistryName(name);
		ForgeRegistries.SOUND_EVENTS.register(event);
		return event;
	}
	
}

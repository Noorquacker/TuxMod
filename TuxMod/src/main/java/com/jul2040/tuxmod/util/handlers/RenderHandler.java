package com.jul2040.tuxmod.util.handlers;

import com.jul2040.tuxmod.Entity.EntityTux;
import com.jul2040.tuxmod.Entity.EntityTuxTamed;
import com.jul2040.tuxmod.Entity.Render.RenderTux;
import com.jul2040.tuxmod.Entity.Render.RenderTuxTamed;

import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class RenderHandler {
	public static void registerEntityRenders()
	{
		RenderingRegistry.registerEntityRenderingHandler(EntityTux.class, new IRenderFactory<EntityTux>()
		{
			@Override
			public Render<? super EntityTux> createRenderFor(RenderManager manager) 
			{
				return new RenderTux(manager);
			}
		});
		RenderingRegistry.registerEntityRenderingHandler(EntityTuxTamed.class, new IRenderFactory<EntityTuxTamed>()
		{
			@Override
			public Render<? super EntityTuxTamed> createRenderFor(RenderManager manager) 
			{
				return new RenderTuxTamed(manager);
			}
		});
}
}

package com.jul2040.tuxmod.util.handlers;

import com.jul2040.tuxmod.util.References;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTableList;

public class LootTableHandler {
	public static final ResourceLocation TUX = LootTableList.register(new ResourceLocation(References.MOD_ID, "tux"));
	public static final ResourceLocation TUX_TAMED = LootTableList.register(new ResourceLocation(References.MOD_ID, "tux_tamed"));
}
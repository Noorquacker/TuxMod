package com.jul2040.tuxmod.util;

public class References {
	public static final String MOD_ID = "tuxmod";
	public static final String NAME = "Tux Mod";
	public static final String VERSION = "1.0";
	public static final String ACCEPTED_VERSIONS = "[1.12.2]";
	public static final String CLIENT_PROXY = "com.jul2040.tuxmod.proxy.ClientProxy";
	public static final String COMMON_PROXY = "com.jul2040.tuxmod.proxy.CommonProxy";
	
	public static final int ENTITY_TUX = 195;
	public static final int ENTITY_TUX_TAMED = 196;
}

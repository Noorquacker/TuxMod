package com.jul2040.tuxmod.util;

import com.jul2040.tuxmod.init.ModItems;

public interface IHasModel {
	public void registerModels();
}
---- Minecraft Crash Report ----
// Who set us up the TNT?

Time: 6/28/18 4:24 PM
Description: Ticking entity

java.lang.NullPointerException: Ticking entity
	at com.jul2040.testmod.Entity.EntityTux.attackEntityFrom(EntityTux.java:70)
	at net.minecraft.entity.EntityLivingBase.onEntityUpdate(EntityLivingBase.java:285)
	at net.minecraft.entity.EntityLiving.onEntityUpdate(EntityLiving.java:247)
	at net.minecraft.entity.Entity.onUpdate(Entity.java:465)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:2313)
	at net.minecraft.entity.EntityLiving.onUpdate(EntityLiving.java:346)
	at net.minecraft.entity.monster.EntityMob.onUpdate(EntityMob.java:57)
	at net.minecraft.world.World.updateEntityWithOptionalForce(World.java:2168)
	at net.minecraft.world.WorldServer.updateEntityWithOptionalForce(WorldServer.java:871)
	at net.minecraft.world.World.updateEntity(World.java:2127)
	at net.minecraft.world.World.updateEntities(World.java:1928)
	at net.minecraft.world.WorldServer.updateEntities(WorldServer.java:643)
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:842)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:743)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:192)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:592)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at com.jul2040.testmod.Entity.EntityTux.attackEntityFrom(EntityTux.java:70)
	at net.minecraft.entity.EntityLivingBase.onEntityUpdate(EntityLivingBase.java:285)
	at net.minecraft.entity.EntityLiving.onEntityUpdate(EntityLiving.java:247)
	at net.minecraft.entity.Entity.onUpdate(Entity.java:465)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:2313)
	at net.minecraft.entity.EntityLiving.onUpdate(EntityLiving.java:346)
	at net.minecraft.entity.monster.EntityMob.onUpdate(EntityMob.java:57)
	at net.minecraft.world.World.updateEntityWithOptionalForce(World.java:2168)
	at net.minecraft.world.WorldServer.updateEntityWithOptionalForce(WorldServer.java:871)
	at net.minecraft.world.World.updateEntity(World.java:2127)

-- Entity being ticked --
Details:
	Entity Type: testmod:tux (com.jul2040.testmod.Entity.EntityTux)
	Entity ID: 84
	Entity Name: entity.Tux.name
	Entity's Exact location: -1463.93, 4.00, 1774.63
	Entity's Block location: World: (-1464,4,1774), Chunk: (at 8,0,14 in -92,110; contains blocks -1472,0,1760 to -1457,255,1775), Region: (-3,3; contains chunks -96,96 to -65,127, blocks -1536,0,1536 to -1025,255,2047)
	Entity's Momentum: 0.00, -0.08, 0.00
	Entity's Passengers: []
	Entity's Vehicle: ~~ERROR~~ NullPointerException: null
Stacktrace:
	at net.minecraft.world.World.updateEntities(World.java:1928)
	at net.minecraft.world.WorldServer.updateEntities(WorldServer.java:643)

-- Affected level --
Details:
	Level name: New World
	All players: 1 total; [EntityPlayerMP['Player344'/129, l='New World', x=-1459.55, y=4.00, z=1771.64]]
	Chunk stats: ServerChunkCache: 657 Drop: 0
	Level seed: 4571753048848495386
	Level generator: ID 01 - flat, ver 0. Features enabled: true
	Level generator options: 
	Level spawn location: World: (-1491,4,1874), Chunk: (at 13,0,2 in -94,117; contains blocks -1504,0,1872 to -1489,255,1887), Region: (-3,3; contains chunks -96,96 to -65,127, blocks -1536,0,1536 to -1025,255,2047)
	Level time: 76530 game time, 8376 day time
	Level dimension: 0
	Level storage version: 0x04ABD - Anvil
	Level weather: Rain time: 9181 (now: true), thunder time: 87083 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: true
Stacktrace:
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:842)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:743)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:192)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:592)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Linux (amd64) version 4.16.0-2-amd64
	Java Version: 1.8.0_171, Oracle Corporation
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 661729256 bytes (631 MB) / 1004011520 bytes (957 MB) up to 1004011520 bytes (957 MB)
	JVM Flags: 2 total; -Xmx1024M -Xms1024M
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.4.2705 5 mods loaded, 5 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State     | ID        | Version      | Source                           | Signature |
	|:--------- |:--------- |:------------ |:-------------------------------- |:--------- |
	| UCHIJAAAA | minecraft | 1.12.2       | minecraft.jar                    | None      |
	| UCHIJAAAA | mcp       | 9.42         | minecraft.jar                    | None      |
	| UCHIJAAAA | FML       | 8.0.99.99    | forgeSrc-1.12.2-14.23.4.2705.jar | None      |
	| UCHIJAAAA | forge     | 14.23.4.2705 | forgeSrc-1.12.2-14.23.4.2705.jar | None      |
	| UCHIJAAAA | testmod   | 1.0          | bin                              | None      |

	Loaded coremods (and transformers): 
	GL info: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
	Profiler Position: N/A (disabled)
	Player Count: 1 / 8; [EntityPlayerMP['Player344'/129, l='New World', x=-1459.55, y=4.00, z=1771.64]]
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fml,forge'
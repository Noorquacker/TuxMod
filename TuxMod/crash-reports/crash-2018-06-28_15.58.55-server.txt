---- Minecraft Crash Report ----
// Oops.

Time: 6/28/18 3:58 PM
Description: Exception generating new chunk

java.lang.OutOfMemoryError: GC overhead limit exceeded
	at net.minecraft.world.chunk.NibbleArray.<init>(NibbleArray.java:13)
	at net.minecraft.world.chunk.storage.ExtendedBlockStorage.<init>(ExtendedBlockStorage.java:39)
	at net.minecraft.world.chunk.Chunk.<init>(Chunk.java:138)
	at net.minecraft.world.gen.ChunkGeneratorFlat.generateChunk(ChunkGeneratorFlat.java:153)
	at net.minecraft.world.gen.ChunkProviderServer.provideChunk(ChunkProviderServer.java:155)
	at net.minecraft.world.World.getChunkFromChunkCoords(World.java:361)
	at net.minecraft.world.ChunkCache.<init>(ChunkCache.java:39)
	at net.minecraft.pathfinding.PathNavigate.getPathToPos(PathNavigate.java:128)
	at net.minecraft.pathfinding.PathNavigateGround.getPathToPos(PathNavigateGround.java:82)
	at net.minecraft.pathfinding.PathNavigate.getPathToXYZ(PathNavigate.java:104)
	at net.minecraft.pathfinding.PathNavigate.tryMoveToXYZ(PathNavigate.java:173)
	at net.minecraft.entity.ai.EntityAIWander.startExecuting(EntityAIWander.java:83)
	at net.minecraft.entity.ai.EntityAITasks.onUpdateTasks(EntityAITasks.java:84)
	at net.minecraft.entity.EntityLiving.updateEntityActionState(EntityLiving.java:843)
	at net.minecraft.entity.EntityLivingBase.onLivingUpdate(EntityLivingBase.java:2580)
	at net.minecraft.entity.EntityLiving.onLivingUpdate(EntityLiving.java:647)
	at net.minecraft.entity.monster.EntityMob.onLivingUpdate(EntityMob.java:49)
	at net.minecraft.entity.monster.EntityZombie.onLivingUpdate(EntityZombie.java:246)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:2396)
	at net.minecraft.entity.EntityLiving.onUpdate(EntityLiving.java:346)
	at net.minecraft.entity.monster.EntityMob.onUpdate(EntityMob.java:57)
	at net.minecraft.world.World.updateEntityWithOptionalForce(World.java:2168)
	at net.minecraft.world.WorldServer.updateEntityWithOptionalForce(WorldServer.java:871)
	at net.minecraft.world.World.updateEntity(World.java:2127)
	at net.minecraft.world.World.updateEntities(World.java:1928)
	at net.minecraft.world.WorldServer.updateEntities(WorldServer.java:643)
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:842)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:743)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:192)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:592)
	at java.lang.Thread.run(Thread.java:748)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Server thread
Stacktrace:
	at net.minecraft.world.chunk.NibbleArray.<init>(NibbleArray.java:13)
	at net.minecraft.world.chunk.storage.ExtendedBlockStorage.<init>(ExtendedBlockStorage.java:39)
	at net.minecraft.world.chunk.Chunk.<init>(Chunk.java:138)
	at net.minecraft.world.gen.ChunkGeneratorFlat.generateChunk(ChunkGeneratorFlat.java:153)

-- Chunk to be generated --
Details:
	Location: -86,218
	Position hash: 940597837738
	Generator: net.minecraft.world.gen.ChunkGeneratorFlat@24196a5f
Stacktrace:
	at net.minecraft.world.gen.ChunkProviderServer.provideChunk(ChunkProviderServer.java:155)
	at net.minecraft.world.World.getChunkFromChunkCoords(World.java:361)
	at net.minecraft.world.ChunkCache.<init>(ChunkCache.java:39)
	at net.minecraft.pathfinding.PathNavigate.getPathToPos(PathNavigate.java:128)
	at net.minecraft.pathfinding.PathNavigateGround.getPathToPos(PathNavigateGround.java:82)
	at net.minecraft.pathfinding.PathNavigate.getPathToXYZ(PathNavigate.java:104)
	at net.minecraft.pathfinding.PathNavigate.tryMoveToXYZ(PathNavigate.java:173)
	at net.minecraft.entity.ai.EntityAIWander.startExecuting(EntityAIWander.java:83)
	at net.minecraft.entity.ai.EntityAITasks.onUpdateTasks(EntityAITasks.java:84)
	at net.minecraft.entity.EntityLiving.updateEntityActionState(EntityLiving.java:843)
	at net.minecraft.entity.EntityLivingBase.onLivingUpdate(EntityLivingBase.java:2580)
	at net.minecraft.entity.EntityLiving.onLivingUpdate(EntityLiving.java:647)
	at net.minecraft.entity.monster.EntityMob.onLivingUpdate(EntityMob.java:49)
	at net.minecraft.entity.monster.EntityZombie.onLivingUpdate(EntityZombie.java:246)
	at net.minecraft.entity.EntityLivingBase.onUpdate(EntityLivingBase.java:2396)
	at net.minecraft.entity.EntityLiving.onUpdate(EntityLiving.java:346)
	at net.minecraft.entity.monster.EntityMob.onUpdate(EntityMob.java:57)
	at net.minecraft.world.World.updateEntityWithOptionalForce(World.java:2168)
	at net.minecraft.world.WorldServer.updateEntityWithOptionalForce(WorldServer.java:871)
	at net.minecraft.world.World.updateEntity(World.java:2127)

-- Entity being ticked --
Details:
	Entity Type: testmod:tux (com.jul2040.testmod.Entity.EntityTux)
	Entity ID: 24856
	Entity Name: entity.Tux.name
	Entity's Exact location: -1464.49, 4.00, 1723.13
	Entity's Block location: World: (-1465,4,1723), Chunk: (at 7,0,11 in -92,107; contains blocks -1472,0,1712 to -1457,255,1727), Region: (-3,3; contains chunks -96,96 to -65,127, blocks -1536,0,1536 to -1025,255,2047)
	Entity's Momentum: 0.00, -0.08, 0.00
	Entity's Passengers: []
	Entity's Vehicle: ~~ERROR~~ NullPointerException: null
Stacktrace:
	at net.minecraft.world.World.updateEntities(World.java:1928)
	at net.minecraft.world.WorldServer.updateEntities(WorldServer.java:643)

-- Affected level --
Details:
	Level name: New World
	All players: 1 total; [EntityPlayerMP['Player233'/102, l='New World', x=-1474.88, y=14.98, z=1726.07]]
	Chunk stats: ServerChunkCache: 35194 Drop: 0
	Level seed: 4571753048848495386
	Level generator: ID 01 - flat, ver 0. Features enabled: true
	Level generator options: 
	Level spawn location: World: (-1491,4,1874), Chunk: (at 13,0,2 in -94,117; contains blocks -1504,0,1872 to -1489,255,1887), Region: (-3,3; contains chunks -96,96 to -65,127, blocks -1536,0,1536 to -1025,255,2047)
	Level time: 71282 game time, 3128 day time
	Level dimension: 0
	Level storage version: 0x04ABD - Anvil
	Level weather: Rain time: 14429 (now: true), thunder time: 92331 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: true
Stacktrace:
	at net.minecraft.server.MinecraftServer.updateTimeLightAndEntities(MinecraftServer.java:842)
	at net.minecraft.server.MinecraftServer.tick(MinecraftServer.java:743)
	at net.minecraft.server.integrated.IntegratedServer.tick(IntegratedServer.java:192)
	at net.minecraft.server.MinecraftServer.run(MinecraftServer.java:592)
	at java.lang.Thread.run(Thread.java:748)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Linux (amd64) version 4.16.0-2-amd64
	Java Version: 1.8.0_171, Oracle Corporation
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 107408384 bytes (102 MB) / 968884224 bytes (924 MB) up to 968884224 bytes (924 MB)
	JVM Flags: 2 total; -Xmx1024M -Xms1024M
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.4.2705 5 mods loaded, 5 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State     | ID        | Version      | Source                           | Signature |
	|:--------- |:--------- |:------------ |:-------------------------------- |:--------- |
	| UCHIJAAAA | minecraft | 1.12.2       | minecraft.jar                    | None      |
	| UCHIJAAAA | mcp       | 9.42         | minecraft.jar                    | None      |
	| UCHIJAAAA | FML       | 8.0.99.99    | forgeSrc-1.12.2-14.23.4.2705.jar | None      |
	| UCHIJAAAA | forge     | 14.23.4.2705 | forgeSrc-1.12.2-14.23.4.2705.jar | None      |
	| UCHIJAAAA | testmod   | 1.0          | bin                              | None      |

	Loaded coremods (and transformers): 
	GL info: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
	Profiler Position: N/A (disabled)
	Player Count: 1 / 8; [EntityPlayerMP['Player233'/102, l='New World', x=-1474.88, y=14.98, z=1726.07]]
	Type: Integrated Server (map_client.txt)
	Is Modded: Definitely; Client brand changed to 'fml,forge'